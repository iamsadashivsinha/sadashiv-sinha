package com.developer.sadashiv.sadashivsinha;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Developer on 17-Jan-18.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public PagerAdapter(FragmentManager fm,CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Bundle args1 = new Bundle();
                args1.putString("NAME", "Sports");
                TabFragment tab1 = new TabFragment();
                tab1.setArguments(args1);
                return tab1;
            case 1:
                Bundle args2 = new Bundle();
                args2.putString("NAME", "Technologies");
                TabFragment tab2 = new TabFragment();
                tab2.setArguments(args2);
                return tab2;
            case 2:
                Bundle args3 = new Bundle();
                args3.putString("NAME", "Design");
                TabFragment tab3 = new TabFragment();
                tab3.setArguments(args3);
                return tab3;
            case 3:
                Bundle args4 = new Bundle();
                args4.putString("NAME", "Politics");
                TabFragment tab4 = new TabFragment();
                tab4.setArguments(args4);
                return tab4;
            case 4:
                Bundle args5 = new Bundle();
                args5.putString("NAME", "Movies");
                TabFragment tab5 = new TabFragment();
                tab5.setArguments(args5);
                return tab5;
            default:
                return null;
        }
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}