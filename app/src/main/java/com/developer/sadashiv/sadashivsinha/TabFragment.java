package com.developer.sadashiv.sadashivsinha;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Developer on 17-Jan-18.
 */

public class TabFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_fragment, container, false);
        TextView tv = v.findViewById(R.id.text_view);
        String mText = getArguments().getString("NAME");
        tv.setText(mText);
        tv.setTypeface(Typeface.createFromAsset(v.getContext().getAssets(),"font/Helvetica Condensed.ttf"));
        return v;
    }
}