package com.developer.sadashiv.sadashivsinha;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    SlidingTabLayout tabLayout;
    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        setupTabs();
    }

    private void initViews() {
        tabLayout = findViewById(R.id.tab_layout);
        pager = findViewById(R.id.pager);
    }

    private void setupTabs(){


        CharSequence Titles[]={"Sports","Technologies","Design","Politics","Movies"};

        PagerAdapter adapter =  new PagerAdapter(getSupportFragmentManager(),Titles,Titles.length);
        
        pager.setAdapter(adapter);
        tabLayout.setViewPager(pager);

    }
}
